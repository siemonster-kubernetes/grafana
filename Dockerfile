FROM grafana/grafana:4.6.3

COPY ./dashboards /etc/grafana/dashboards
COPY ./datasources /etc/grafana/datasources
COPY ./setup.sh /setup.sh
COPY ./grafana_icon.svg /usr/share/grafana/public/img/

ENTRYPOINT ["/setup.sh"]
